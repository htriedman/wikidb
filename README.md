# wikidb

Repo for compiling, preprocessing, augmenting, etc. a series of queries (along with query titles and descriptions) from https://quarry.wmcloud.org, with the ultimate intention of training a ML model that can be prompted write queries on the Mediawiki table ecosystem.

Data lives on huggingface at: https://huggingface.co/datasets/htriedman/wikidb
